import React from "react";
import "./Button.scss";
import PropTypes from "prop-types";

const Button = ({ variant, children, onClick }) => {
  return (
    <button className={variant} onClick={onClick}>
      {children}
    </button>
  );
};

Button.propTypes = {
  variant: PropTypes.oneOf(["primary", "secondary"]),
};

export default Button;
