import "./Counter.scss";
import { useDispatch, useSelector } from "react-redux";
import Button from "../Button";
import { increment, decrement } from "../../store/counter/counterActions";

const Counter = () => {
  const count = useSelector((state) => state.count.value);
  const dispatch = useDispatch();
  const handleIncrement = () => dispatch(increment());
  const handleDecremeent = () => dispatch(decrement());

  return (
    <div className="container">
      <div className="counter">
        Count is : <span>{count}</span>
      </div>
      <Button
        variant="secondary"
        onClick={handleIncrement}
        children="Increment +"
      ></Button>
      <Button
        variant="primary"
        onClick={handleDecremeent}
        children="Decrement +"
      ></Button>
    </div>
  );
};

export default Counter;
