import { combineReducers } from "redux";
import { reducer } from "./counterReducer";

export const reducers = combineReducers({
  count: reducer,
});
