import { legacy_createStore as createStore } from "redux";
import { reducers } from "./counter/reducers";
export const store = createStore(reducers);
